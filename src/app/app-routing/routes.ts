import { Routes } from'@angular/router';
import {NewEntryComponent} from '../new-entry/new-entry.component';
import { UploadComponent} from '../upload/upload.component';
import { ViewEntriesComponent} from '../view-entries/view-entries.component';



export const routes:Routes = [
    {  path:'home', component: NewEntryComponent},
    {  path:'upload', component:UploadComponent},   
    {  path:'viewentries', component:ViewEntriesComponent},   
    {  path:'', redirectTo:'/home', pathMatch:'full'}    
];