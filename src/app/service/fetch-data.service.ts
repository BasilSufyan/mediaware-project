import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';

import {hostname} from '../Shared/hostname';
import { EntryFrom } from '../Shared/EntryFrom';
import {ExcelData} from '../Shared/Exceldata';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',    
  })
};

@Injectable()
export class FetchDataService {

  host = hostname;
  constructor(private http:HttpClient) { }  

  getData(){
    return this.http.get<ExcelData[]>(this.host);
  }

  postData(data:ExcelData[]){    
    //var dataa = {"site":"JAGSHWARA - KURNOOL","roToCompany":"MOTS MEDIA & ENTERTAINMENT PVT. LTD.","adtype":null,"area":null,"scheduledFrom":"17/12/2015","scheduledTo":"2019-01-01","rate":5000.0,"unit":15.0,"company":"MOTIF  &  NTERTAINMENT PVT. LTD.","duration":50.0,"addDesc":"Something","addLan":"Chin","rentUnit":"Days","caption":"8070"};
    return this.http.post(this.host, data, httpOptions);   
  }

  // private handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError(
  //     'Something bad happened; please try again later.');
  // };
}
