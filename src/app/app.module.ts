import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { FetchDataService} from '../app/service/fetch-data.service';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AppComponent } from './app.component';
import { NewEntryComponent } from './new-entry/new-entry.component';
import { MatInputModule,MatButtonModule,MatDatepickerModule,MatDialogModule,MatFormFieldModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppRoutingModule} from '../app/app-routing/approuting.module';
import { UploadComponent } from './upload/upload.component';
import { ViewEntriesComponent } from './view-entries/view-entries.component';

@NgModule({
  declarations: [
    AppComponent,
    NewEntryComponent,    
    UploadComponent, ViewEntriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule, 
    MatFormFieldModule,
    MatDatepickerModule,
    MatDialogModule, 
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [FetchDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
