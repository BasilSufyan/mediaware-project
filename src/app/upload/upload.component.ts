import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { ExcelData } from '../Shared/Exceldata';
import { EntryFrom } from '../Shared/EntryFrom';
import { FetchDataService } from '../service/fetch-data.service';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  @ViewChild('uploadfile') uploadfile;
  data: ExcelData[];
  valid: boolean = false;
  Errors = new Array;
  error:boolean = false;
  constructor(private FetdataSvc:FetchDataService,private router: Router,private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }

  uploadFile(event) {
    this.Errors = [];
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      /* save data */
      this.data = <ExcelData[]>XLSX.utils.sheet_to_json(ws, { raw: true })
      this.validateFile(this.data);

    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateFile(filedata: ExcelData[]) {
    
    let formdata: EntryFrom = JSON.parse(sessionStorage.getItem('formdata'));
    console.log("Form data", formdata);
    let startdate = new Date(formdata.StartDate);
    let enddate = new Date(formdata.EndDate);
    
    let Convertedstartdate: Number = this.converttoExcelDate(startdate);
    let Convertedenddate: Number = this.converttoExcelDate(enddate);
    let costEntered = formdata.Cost;
    let totalCost = 0;
    this.valid = true;

    filedata.forEach((row, index) => {
      let ScheduledFrom = Math.floor(Number(row['ScheduledFrom']));
      let ScheduledTo = Math.floor(Number(row['ScheduledTo']));
      let rate = row["Rate"];
      let unit = row["Unit"];
      let cost: number = (rate * unit);
      totalCost += cost;
      if (Math.floor(ScheduledFrom) < Convertedstartdate) {
        // Error
        console.log("Scheduled from is less tha start date" + Math.floor(ScheduledFrom) + " " + Convertedstartdate);
        this.Errors.push("Scheduled from date is less than start date. Row:" + index);
        this.valid = false;
        this.error = true;
      }
      else{        
         this.data[index].ScheduledFrom = this.ExcelDateToJSDate(ScheduledFrom);
      }
      if (Math.floor(ScheduledTo) < Convertedenddate) {
        //Error
        console.log("Scheduled To is less tha End date" + Math.floor(ScheduledTo) + " " + Convertedenddate);
        this.Errors.push("Scheduled To date is less than End date. Row:" + index);
        this.valid = false;
        this.error = true;
      }
      else{
        this.data[index].ScheduledTo = this.ExcelDateToJSDate(ScheduledTo);
      }

      

    });

    if (totalCost != costEntered) {
      this.valid = false;
      this.error = true;
      console.log('Estimate cost not matching the total cost in the workbook');
      this.Errors.push("Estimate cost not matching the total cost in the workbook");
    }
    console.log(this.Errors);
  }

  converttoExcelDate(date: Date): Number {

    try {

      let year = date.getUTCFullYear();
      let days = ((year - 1900) * 365) + Math.floor((year - 1900) / 4);

      let month = date.getMonth();
      let day = date.getDay();
      for (let i = 1; i < month; i++) {
        if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12) days += 31;
        else if (i == 2) {
          if (year % 4 == 0) days += 29;
          else days += 28;
        }
        else if (i == 4 || i == 6 || i == 9 || i == 11) days += 30;
      }
      days += day;
      return days;
    }

    catch (ex) {
      console.log("error while converting date", ex);
    }
  }

  ExcelDateToJSDate(date) {
    return new Date(Math.round((date - 25569)*86400*1000)).toLocaleDateString();
  }

  saveFile(event) {
      // submit this.data to server side and store it in a database
      this.spinnerService.show();
      console.log(this.data);
      this.FetdataSvc.postData(this.data)
      .subscribe((data) => {
        console.log("Post: ",data);   
        this.spinnerService.hide();
        
      });
      
  }
  ViewDetails(event){
    this.router.navigate(['viewentries']);
  }
}
