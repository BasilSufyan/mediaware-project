import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntryFrom } from '../Shared/EntryFrom';
import { FetchDataService } from '../service/fetch-data.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-new-entry',
  templateUrl: './new-entry.component.html',
  styleUrls: ['./new-entry.component.css']
})
export class NewEntryComponent implements OnInit {


  entryform: FormGroup;
  formdata: EntryFrom;
  hidden = false;
  flag: Boolean = true;
  formErrors = {
    'Name': '',
    'StartDate': '',
    'EndDate': '',
    'Cost': '',
    'objective': '',
    'clientname': '',
    'brandname': ''
  };

  validationMessages = {
    'Name': {
      'required': 'Estimate Name is required.',
      'pattern': 'Duplicate names present',
    },
    'Cost': {
      'required': 'Estimate Cost is required.',
      'pattern': 'Only decimal values accepted',
    },
  }

  clientName = ['amazon', 'microsoft', 'google'];
  brandName = ['titan', 'rolex', 'tf']

  invalid: boolean = true;


  constructor(private fb: FormBuilder, private data: FetchDataService, private router: Router) {
    this.hidden = false;
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.entryform = this.fb.group({
      Name: ['', [Validators.required]],
      StartDate: [Validators.required],
      EndDate: [Validators.required],
      Cost: ['', [Validators.required, Validators.pattern]],
      objective: [],
      clientname: [],
      brandname: []
    });
    this.entryform.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }


  onValueChanged(data?: any) {
    if (!this.entryform) { return }

    const form = this.entryform;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      if (field != 'Name') {
        this.formErrors[field] = '';
        const control = form.get(field);

        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
          }
        }
      }

    }

  }
  onSubmit() {
    this.formdata = this.entryform.value;
    // save to session storage
    console.log(this.formdata);
    sessionStorage.setItem("formdata", JSON.stringify(this.formdata));
    this.router.navigate(['upload']);
  }

  onblurname(event) {

    var split = event.target.value.split(' ');
    this.flag = true;
    for (let i = 0; i < split.length; i++) {
      let word = split[i];
      for (let j = i + 1; j < split.length; j++) {
        if (word == split[j]) {
          this.flag = false;
          break;
        }
      }
    }

    if (this.flag == false) {  
          
      this.formErrors['Name'] = this.validationMessages['Name']['pattern']; + ' ';
    }

  }

}
