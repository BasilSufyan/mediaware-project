import { Component, OnInit } from '@angular/core';
import { ExcelData } from '../Shared/Exceldata';
import { FetchDataService } from '../service/fetch-data.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-entries',
  templateUrl: './view-entries.component.html',
  styleUrls: ['./view-entries.component.css']
})
export class ViewEntriesComponent implements OnInit {

  estimates:ExcelData[];
  currentEstimate:ExcelData;
  closeResult: string;

  constructor(private fetchSvc:FetchDataService, private modalService: NgbModal) { 
      this.fetchSvc.getData()
      .subscribe((data) => {this.estimates = data; console.log("GET : fetched",this.estimates)});
  }

  ngOnInit() {
  }
  
  openVerticallyCentered(content,i) {
    console.log(this.estimates[i]);
    this.currentEstimate = this.estimates[i];
    this.modalService.open(content, { centered: true });
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
