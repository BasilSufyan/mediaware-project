export class ExcelData {
    Adtype: string;
    Company: string;
    Duration: number;
    Rate: number;
    RentUnit: string;
    RoToCompany: string;
    ScheduledFrom: string;
    ScheduledTo: string;
    Site: string;
    Unit: number;
    addDesc: string;
    addLan: string;
    caption: number;
}